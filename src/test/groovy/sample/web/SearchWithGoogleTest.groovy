package sample.web

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

/**
 * Googleで検索テストシナリオをJUnitテストケースとして実行。
 * @author nabla2.metamodel.generator
 */
@RunWith(Cucumber)
@CucumberOptions(plugin = [
  "pretty", "html:build/reports/tests/cucumber"
])
class SearchWithGoogleTest {
}