package sample.web

import cucumber.api.Scenario
import cucumber.api.groovy.EN
import cucumber.api.groovy.Hooks
import geb.Browser
import geb.Page
import nabla2.automation.step.AssertResultValueIs
import nabla2.automation.step.NOP
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Wait
import nabla2.automation.step.ui.AssertPageShown
import nabla2.automation.step.ui.Choose
import nabla2.automation.step.ui.Click
import nabla2.automation.step.ui.OpenPage
import nabla2.automation.step.ui.TypeText
import org.openqa.selenium.chrome.ChromeDriver

/**
 * Googleで検索テストシナリオ用ステップ定義
 * @author nabla2.metamodel.generator
 */
this.metaClass.mixin(Hooks)
this.metaClass.mixin(EN)

class メインページ extends Page {
  static url = '/'
  static content = {
    検索ワード { $("""#lst-ib""") }
    次ページリンク { $("""#pnnext""") }
    Googleロゴ { $("""#logocont a""") }
  }
}
class Web用ホームページ extends Page {
  static url = '/webhp'
  static content = {
    プライバシーポリシーリンク { $("""#fsr > a:nth-child(1)""") }
    利用規約リンク { $("""#fsr > a:nth-child(2)""") }
  }
}
class プライバシーポリシーページ extends Page {
  static url = '/intl/{lang}/policies/privacy/'
  static content = {

  }
}
class 利用規約ページ extends Page {
  static url = '/intl/{lang}/policies/terms/regional.html'
  static content = {

  }
}

System.getProperty('os.name').with {
  String ext = it.toLowerCase().contains('win') ? '.exe' : ''
  System.setProperty(
    'webdriver.chrome.driver', "./build/webdriver/chromedriver${ext}"
  )
}

ScenarioContext context = new ScenarioContext(
  new Browser(
    driver  :new ChromeDriver(),
    baseUrl :'http://www.google.co.jp/',
  ),
  [
    'メインページ': new メインページ(),
    'Web用ホームページ': new Web用ホームページ(),
    'プライバシーポリシーページ': new プライバシーポリシーページ(),
    '利用規約ページ': new 利用規約ページ(),
  ],
  [:],
)

Before() { Scenario scenario ->
  context.cucumberScenario = scenario
}

After() { Scenario scenario ->
  if (scenario.failed) context.takeScreenshot()
}

static String v(String str) {
  str?.replaceAll(/^:/, '')
}

/**
 * 【秒数】秒間待機する
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Q秒間待機する\E((?:【[^【】]+】)*)$/) {
  String 秒数,
  String o
  -> new Wait(v(秒数)).execute(context.opts(o))
}
/**
 * なにもしない
 */
When(~/^\Qなにもしない\E((?:【[^【】]+】)*)$/) {
  String o
  -> new NOP().execute(context.opts(o))
}
/**
 * 【画面名】を開く
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Qを開く\E((?:【[^【】]+】)*)$/) {
  String 画面名,
  String o
  -> new OpenPage(v(画面名)).execute(context.opts(o))
}
/**
 * 【画面項目名】に【文字列】と入力する
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Qに\E\s*【(:[^【】]*|[^【:】]*)】\s*\Qと入力する\E((?:【[^【】]+】)*)$/) {
  String 画面項目名,
  String 文字列,
  String o
  -> new TypeText(v(画面項目名), v(文字列)).execute(context.opts(o))
}
/**
 * 【画面項目名】をクリックする
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Qをクリックする\E((?:【[^【】]+】)*)$/) {
  String 画面項目名,
  String o
  -> new Click(v(画面項目名)).execute(context.opts(o))
}
/**
 * 【画面項目名】から【選択項目名or値】を選択する
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Qから\E\s*【(:[^【】]*|[^【:】]*)】\s*\Qを選択する\E((?:【[^【】]+】)*)$/) {
  String 画面項目名,
  String 選択項目名or値,
  String o
  -> new Choose(v(画面項目名), v(選択項目名or値)).execute(context.opts(o))
}
/**
 * 【画面名】に遷移している
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Qに遷移している\E((?:【[^【】]+】)*)$/) {
  String 画面名,
  String o
  -> new AssertPageShown(v(画面名)).execute(context.opts(o))
}
/**
 * 【結果変数名】の値が【期待値】である
 */
When(~/^\s*【(:[^【】]*|[^【:】]*)】\s*\Qの値が\E\s*【(:[^【】]*|[^【:】]*)】\s*\Qである\E((?:【[^【】]+】)*)$/) {
  String 結果変数名,
  String 期待値,
  String o
  -> new AssertResultValueIs(v(結果変数名), v(期待値)).execute(context.opts(o))
}