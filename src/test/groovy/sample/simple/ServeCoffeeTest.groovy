package sample.simple

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

/**
 * Serve coffeeテストシナリオをJUnitテストケースとして実行。
 * @author nabla2.metamodel.generator
 */
@RunWith(Cucumber)
@CucumberOptions(plugin = [
  "pretty", "html:build/reports/tests/cucumber"
])
class ServeCoffeeTest {
}