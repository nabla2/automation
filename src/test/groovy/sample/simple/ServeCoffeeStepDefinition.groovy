package sample.simple

import cucumber.api.Scenario
import cucumber.api.groovy.EN
import cucumber.api.groovy.Hooks
import geb.Browser
import geb.Page
import nabla2.automation.step.NOP
import nabla2.automation.step.ScenarioContext
import org.openqa.selenium.chrome.ChromeDriver

/**
 * Serve coffeeテストシナリオ用ステップ定義
 * @author nabla2.metamodel.generator
 */
this.metaClass.mixin(Hooks)
this.metaClass.mixin(EN)

  

System.getProperty('os.name').with {
  String ext = it.toLowerCase().contains('win') ? '.exe' : ''
  System.setProperty(
    'webdriver.chrome.driver', "./build/webdriver/chromedriver${ext}"
  )
}

ScenarioContext context = new ScenarioContext(
  new Browser(
    driver  :new ChromeDriver(),
    baseUrl :'',
  ),
  [
  :
  ],
  [:],
)

Before() { Scenario scenario ->
  context.cucumberScenario = scenario
}

After() { Scenario scenario ->
  if (scenario.failed) context.takeScreenshot()
}

String v(String str) {
  str?.replaceAll(/^:/, '')
}

/**
 * there are 【coffees】 coffees left in the machine
 */
When(~/^\Qthere are\E\s*【(:[^【】]*|[^【:】]*)】\s*\Qcoffees left in the machine\E((?:【[^【】]+】)*)$/) {
  String coffees,
  String o
  -> new NOP(v(coffees)).execute(context.opts(o))
}
/**
 * I have deposited 【money_amount】$
 */
When(~/^\QI have deposited\E\s*【(:[^【】]*|[^【:】]*)】\s*\Q$\E((?:【[^【】]+】)*)$/) {
  String money_amount,
  String o
  -> new NOP(v(money_amount)).execute(context.opts(o))
}
/**
 * I press the coffee button
 */
When(~/^\QI press the coffee button\E((?:【[^【】]+】)*)$/) {
  String o
  -> new NOP().execute(context.opts(o))
}
/**
 * I should be served a coffee
 */
When(~/^\QI should be served a coffee\E((?:【[^【】]+】)*)$/) {
  String o
  -> new NOP().execute(context.opts(o))
}