package nabla2.automation.template

import nabla2.automation.AutomationEntityLoader
import nabla2.automation.model.Feature

class StepDefinitionGroovyTest extends GroovyTestCase {
  AutomationEntityLoader loader = AutomationEntityLoader.from(
    Class.getResourceAsStream('/scenario.xlsx')
  )

  void testGeneratingGlueCodeFromStepDefinitions() {
    loader.constraintViolation$.test().values().with {
      assert it.empty
    }

    loader.feature$.test().values().with {
      assert it.size() == 1
      Feature feature = it.first()
      assert feature.stepDefinitions.size() == 4
      StepDefinitionGroovy template = new StepDefinitionGroovy(feature)
      assert template.relPath == 'sample/simple/ServeCoffeeStepDefinition.groovy'
      template.source.with {
        assert it.contains("""\
        |package sample.simple
        |
        |import cucumber.api.Scenario
        |import cucumber.api.groovy.EN
        |import cucumber.api.groovy.Hooks
        |import geb.Browser
        |import geb.Page
        |import nabla2.automation.step.NOP
        |import nabla2.automation.step.ScenarioContext
        |import org.openqa.selenium.chrome.ChromeDriver
        """.stripMargin().trim())

        assert it.contains("""\
        |this.metaClass.mixin(Hooks)
        |this.metaClass.mixin(EN)
        """.stripMargin().trim())

        assert it.contains("""\
        |When(~/^\\Qthere are\\E\\s*【(:[^【】]*|[^【:】]*)】\\s*\\Qcoffees left in the machine\\E((?:【[^【】]+】)*)\$/) {
        |  String coffees,
        |  String o
        |  -> new NOP(v(coffees)).execute(context.opts(o))
        |}
        """.stripMargin().trim())

        assert it.contains("""\
        |When(~/^\\QI have deposited\\E\\s*【(:[^【】]*|[^【:】]*)】\\s*\\Q\$\\E((?:【[^【】]+】)*)\$/) {
        |  String money_amount,
        |  String o
        |  -> new NOP(v(money_amount)).execute(context.opts(o))
        |}
        """.stripMargin().trim())
      }
    }
  }
}
