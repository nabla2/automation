package nabla2.automation.template

import nabla2.automation.AutomationEntityLoader
import nabla2.automation.model.Feature

class JunitRunnerGroovyTest extends GroovyTestCase {
  AutomationEntityLoader loader = AutomationEntityLoader.from(
    Class.getResourceAsStream('/scenario.xlsx')
  )

  void testGeneratingJunitRunnerClassFile() {
    loader.constraintViolation$.test().values().with {
      assert it.empty
    }

    loader.feature$.test().values().with {
      assert it.size() == 1
      Feature feature = it.first()
      JunitRunnerGroovy template = new JunitRunnerGroovy(feature)
      assert template.relPath == 'sample/simple/ServeCoffeeTest.groovy'
      template.source.with {
        assert it.contains("""
        |package sample.simple
        |
        |import cucumber.api.CucumberOptions
        |import cucumber.api.junit.Cucumber
        |import org.junit.runner.RunWith
        """.stripMargin().trim())

        assert it.contains("""
        |@RunWith(Cucumber)
        |@CucumberOptions(plugin = [
        |  "pretty", "html:build/reports/tests/cucumber"
        |])
        |class ServeCoffeeTest {
        |}
        """.stripMargin().trim())
      }
    }
  }
}
