package nabla2.automation.template

import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.automation.AutomationEntityLoader
import nabla2.automation.model.Feature
import nabla2.excel.ExcelWorkbookLoader

class CucumberScenarioFeatureTest extends GroovyTestCase {


  AutomationEntityLoader loader = AutomationEntityLoader.from(
    Class.getResourceAsStream('/scenario.xlsx')
  )

  void testGenerateTheSimplestFeatureFile() {

    loader.constraintViolation$.test().values().with {
      assert it.empty
    }

    loader.feature$.test().values().with {
      assert it.size() == 1
      Feature feature = it.first()
      assert feature.scenarios.size() == 1
      CucumberScenarioFeature template = new CucumberScenarioFeature(feature)
      assert template.relPath == 'sample/simple/ServeCoffee.feature'
      template.source.with {
        assert it.contains("""\
        |Feature: Serve coffee
        |  Coffee should not be served until paid for
        |  Coffee should not be served until the button has been pressed
        |  If there is no coffee left then money should be refunded
        """.stripMargin().trim())

        assert it.contains("""\
        |Scenario: Buy last coffee
        |  Given there are 【1】 coffees left in the machine
        |  Given I have deposited 【1】\$
        |  When I press the coffee button
        |  Then I should be served a coffee
        """.stripMargin().trim())
      }
    }
  }
}
