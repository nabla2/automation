#
# Googleで検索テストシナリオ
# @author nabla2.metamodel.generator
#
Feature: Googleで検索
  

Scenario Outline: キーワード1つで検索
  When 【メインページ】を開く
  When 【検索ワード】に【hogehoge】と入力する【タイムアウト:30】【スクリーンショット】
  When 【次ページリンク】をクリックする
  When 【Googleロゴ】をクリックする
  When 【Web用ホームページ】に遷移している【スクリーンショット】
  When 【<機能リンク>】をクリックする
  When 【<機能リンク遷移先>】に遷移している【スクリーンショット】【結果保管先:言語】
  When 【言語】の値が【["ja"]】である

  Examples:
  | 機能リンク | 機能リンク遷移先 |
  | プライバシーポリシーリンク | プライバシーポリシーページ |
  | 利用規約リンク | 利用規約ページ |