package nabla2.gradle

import org.gradle.api.Project
import nabla2.gradle.BasePlugin
import nabla2.automation.gradle.GenerateAutomationScriptTask

/**
 * 自動操作自動生成Gradleプラグイン
 *
 * @author nabla2.metamodel.generator
 */
class AutomationGradlePlugin extends BasePlugin {

  void apply(Project project) {
    register(
      project,
      GenerateAutomationScriptTask,
      GenerateAutomationScriptTask.Settings,
    )
  }
}