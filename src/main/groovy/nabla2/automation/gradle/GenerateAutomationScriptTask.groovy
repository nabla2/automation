package nabla2.automation.gradle

import io.reactivex.Observable
import nabla2.automation.model.Feature
import nabla2.automation.model.FeatureValidator
import nabla2.automation.model.ScenarioPlaceholderValidator
import nabla2.automation.model.ScenarioValidator
import nabla2.automation.model.StepDefinitionValidator
import nabla2.automation.model.StepValidator
import nabla2.automation.model.ui.PageItemValidator
import nabla2.automation.model.ui.PageValidator
import nabla2.automation.template.CucumberScenarioFeature
import nabla2.automation.template.JunitRunnerGroovy
import nabla2.automation.template.StepDefinitionGroovy
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.Table
import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.TaskAction
/**
 * 自動操作スクリプトを生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateAutomationScriptTask extends DefaultTask {

  @InputFiles FileCollection workbooks
  String description = '自動操作スクリプトを生成'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with {
      validate(it)
      Feature.from(it).subscribe {
        new CucumberScenarioFeature(it)
           .generateTo(new File('src/test/resources'))
      }
      Feature.from(it).subscribe {
        new StepDefinitionGroovy(it)
           .generateTo(new File('src/test/groovy'))
      }
      Feature.from(it).subscribe {
        new JunitRunnerGroovy(it)
           .generateTo(new File('src/test/groovy'))
      }
    }
  }

  void validate(Observable<Table> table$) {
    Observable.fromArray(
      new FeatureValidator().validate(table$),
      new ScenarioValidator().validate(table$),
      new ScenarioPlaceholderValidator().validate(table$),
      new StepValidator().validate(table$),
      new StepDefinitionValidator().validate(table$),
      new PageValidator().validate(table$),
      new PageItemValidator().validate(table$),
    )
    .flatMap {it}
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new IllegalStateException(
        "Invalid records are found in ${workbooks.collect{it.canonicalPath}.join(', ')}: ${it}"
      )
    }
  }

  static class Settings extends TaskSettings<GenerateAutomationScriptTask> {
    FileCollection workbooks
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateAutomationScriptTask task) {
      task.workbooks = workbooks
    }
  }
}