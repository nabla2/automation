package nabla2.automation.gradle

import io.reactivex.Observable
import nabla2.automation.model.Feature
import nabla2.automation.model.FeatureValidator
import nabla2.automation.model.ScenarioValidator
import nabla2.automation.model.StepDefinitionValidator
import nabla2.automation.model.StepValidator
import nabla2.automation.template.StepDefinitionGroovy
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.Table
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.TaskAction
/**
 * 自動操作ステップ定義を生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateStepDefinitionGroovy extends DefaultTask {

  @InputFile File workbook = null
  String description = '自動操作ステップ定義を生成'

  @TaskAction
  void exec() {
    ExcelWorkbookLoader.from(workbook).table$.with {
      validate(it)
      Feature.from(it).subscribe {
        new StepDefinitionGroovy(it)
           .generateTo(new File('src/test/groovy'))
      }
    }
  }

  void validate(Observable<Table> table$) {
    Observable.fromArray(
      new FeatureValidator().validate(table$),
      new ScenarioValidator().validate(table$),
      new StepValidator().validate(table$),
      new StepDefinitionValidator().validate(table$),
    )
    .flatMap {it}
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new IllegalStateException(
        "Invalid records are found in ${workbook.canonicalPath}: ${it}"
      )
    }
  }

  static class Settings extends TaskSettings<GenerateStepDefinitionGroovy> {
    File workbook
    @Override
    void setup(GenerateStepDefinitionGroovy task) {
      task.workbook = workbook
    }
  }
}