package nabla2.automation

import groovy.transform.Canonical
import io.reactivex.Observable
import nabla2.automation.model.Feature
import nabla2.automation.model.FeatureValidator
import nabla2.automation.model.Scenario
import nabla2.automation.model.ScenarioPlaceholder
import nabla2.automation.model.ScenarioPlaceholderValidator
import nabla2.automation.model.ScenarioValidator
import nabla2.automation.model.Step
import nabla2.automation.model.StepDefinition
import nabla2.automation.model.StepDefinitionValidator
import nabla2.automation.model.StepValidator
import nabla2.automation.model.ui.Page
import nabla2.automation.model.ui.PageItem
import nabla2.automation.model.ui.PageItemValidator
import nabla2.automation.model.ui.PageValidator
import nabla2.excel.ExcelWorkbookLoader
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation

/**
 * 自動操作エンティティローダ
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class AutomationEntityLoader {

  final Observable<Table> table$

  private AutomationEntityLoader(File file) {
    table$ = ExcelWorkbookLoader.from(file).table$
  }

  private AutomationEntityLoader(InputStream stream) {
    table$ = ExcelWorkbookLoader.from(stream).table$
  }

  static AutomationEntityLoader from(File file) {
    new AutomationEntityLoader(file)
  }

  static AutomationEntityLoader from(InputStream stream) {
    new AutomationEntityLoader(stream)
  }

  Observable<Feature> getFeature$() {
    Feature.from(table$)
  }
  Observable<Scenario> getScenario$() {
    Scenario.from(table$)
  }
  Observable<ScenarioPlaceholder> getScenarioPlaceholder$() {
    ScenarioPlaceholder.from(table$)
  }
  Observable<Step> getStep$() {
    Step.from(table$)
  }
  Observable<StepDefinition> getStepDefinition$() {
    StepDefinition.from(table$)
  }
  Observable<Page> getPage$() {
    Page.from(table$)
  }
  Observable<PageItem> getPageItem$() {
    PageItem.from(table$)
  }

  Observable<ConstraintViolation<?>> getConstraintViolation$() {
    Observable.fromArray(
      new FeatureValidator().validate(table$),
      new ScenarioValidator().validate(table$),
      new ScenarioPlaceholderValidator().validate(table$),
      new StepValidator().validate(table$),
      new StepDefinitionValidator().validate(table$),
      new PageValidator().validate(table$),
      new PageItemValidator().validate(table$),
    ).flatMap{ it }
  }

}