package nabla2.automation.template

import groovy.transform.Canonical
import nabla2.automation.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class JunitRunnerGroovy implements TextFileTemplate {
  Feature feature

  @Override
  String getRelPath() {
    return "${feature.fqn.replace('.', '/')}Test.groovy"
  }

  @Override
  String getSource() {
  """\
  |package ${feature.namespace}
  |
  |import cucumber.api.CucumberOptions
  |import cucumber.api.junit.Cucumber
  |import org.junit.runner.RunWith
  |
  |/**
  | * ${feature.name}テストシナリオをJUnitテストケースとして実行。
  | * ${regenerationMark}
  | */
  |@RunWith(Cucumber)
  |@CucumberOptions(plugin = [
  |  "pretty", "html:build/reports/tests/cucumber"
  |])
  |class ${feature.className}Test {
  |}
  """.stripMargin().trim()
  }

}
