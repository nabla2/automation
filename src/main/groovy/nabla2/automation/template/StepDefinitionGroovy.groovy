package nabla2.automation.template

import groovy.transform.Canonical
import nabla2.automation.model.Feature
import nabla2.automation.step.ScenarioContext
import nabla2.generator.TextFileTemplate

import java.util.regex.Pattern

@Canonical
class StepDefinitionGroovy implements TextFileTemplate {

  Feature feature

  List<String> getImportedModules() {
    (['cucumber.api.groovy.EN',
      'cucumber.api.groovy.Hooks',
      'cucumber.api.Scenario',
      'geb.Browser',
      'geb.Page',
      'org.openqa.selenium.chrome.ChromeDriver',
      ScenarioContext.canonicalName,
    ] + feature.stepDefinitions.collect{ it.fqn })
    .sort()
    .unique()
  }

  @Override
  String getSource() {
  """\
  |package ${feature.namespace}
  |
  ${importedModules.collect{"""\
  |import $it
  """.trim()}.join('\n')}
  |
  |/**
  | * ${feature.name}テストシナリオ用ステップ定義
  | * ${regenerationMark}
  | */
  |this.metaClass.mixin(Hooks)
  |this.metaClass.mixin(EN)
  |
  ${feature.pages.collect{ page ->
  """\
  |class ${page.name} extends Page {
  |  static url = '${page.path}'
  |  static content = {
  ${page.items.collect{ item ->
  """\
  |    ${item.name} { \$(""\"${item.selector}""\") }
  """.trim()}.join('\n')}
  |  }
  |}
  """.trim()}.join('\n')}
  |
  |System.getProperty('os.name').with {
  |  String ext = it.toLowerCase().contains('win') ? '.exe' : ''
  |  System.setProperty(
  |    'webdriver.chrome.driver', "./build/webdriver/chromedriver\${ext}"
  |  )
  |}
  |
  |ScenarioContext context = new ScenarioContext(
  |  new Browser(
  |    driver  :new ChromeDriver(),
  |    baseUrl :'${feature.url}',
  |  ),
  |  [
  ${feature.pages.with {
  if (it.empty) return ':'
  it.collect{ page ->
  """\
  |    '${page.name}': new ${page.name}(),
  """.trim()}.join('\n')
  }}
  |  ],
  |  [:],
  |)
  |
  |Before() { Scenario scenario ->
  |  context.cucumberScenario = scenario
  |}
  |
  |After() { Scenario scenario ->
  |  if (scenario.failed) context.takeScreenshot()
  |}
  |
  |static String v(String str) {
  |  str?.replaceAll(/^:/, '')
  |}
  |
  ${feature.stepDefinitions.collect{ definition ->
    Pattern format = definition.description.get()
    List<String> params = definition.description.params
  """
  |/**
  | * ${definition.description}
  | */
  |When(~/^${format.toString()}((?:【[^【】]+】)*)\$/) {
  ${(params.collect{ """\
  |  String ${it}
  """.trim()} + ['|  String o']).join(',\n')}
  |  -> new ${definition.identifier}(${params.collect{"v(${it})"}.join(', ')}).execute(context.opts(o))
  |}
  """.trim()}.join('\n')
  }
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    "${feature.fqn.replace('.', '/')}StepDefinition.groovy"
  }
}
