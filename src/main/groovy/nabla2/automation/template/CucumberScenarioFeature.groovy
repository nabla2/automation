package nabla2.automation.template

import groovy.transform.Canonical
import nabla2.automation.model.Feature
import nabla2.automation.model.Scenario
import nabla2.automation.model.ScenarioPlaceholder
import nabla2.automation.model.Step
import nabla2.generator.TextFileTemplate

@Canonical
class CucumberScenarioFeature implements TextFileTemplate {

  Feature feature

  String getSource() {
  """\
  |#
  |# ${feature.name}テストシナリオ
  |# ${regenerationMark}
  |#
  |Feature: ${feature.name}
  ${feature.comment.empty ? '' : """\
  |  ${feature.comment.get().replaceAll(/\n/, '\n  ').trim()}
  """.trim()}
  |
  ${feature.scenarios.collect{ scenario -> """\
  |Scenario${usesTemplate(scenario) ? ' Outline' : ''}: ${scenario.name}
  ${scenario.steps.collect{ step -> """\
  |  ${categoryOf(step)} ${step.description}${opts(step)}
  """.trim()}.join('\n')}
  |
  ${exampleTableOf(scenario)}
  """.trim()}.join('\n')}
  """.trim().stripMargin()
  }

  static String exampleTableOf(Scenario scenario) {
    if (!usesTemplate(scenario)) return ''
    List<ScenarioPlaceholder> examples = scenario.placeholders
    List<String> header = examples.collect{ it.name.get() }.unique()
    Map<Integer, List<ScenarioPlaceholder>> rows = examples.groupBy{ it.caseNumber.get() }
  """\
  |  Examples:
  |  | ${header.join(' | ')} |
  ${rows.collect{ caseNumber, row -> """\
  |  | ${header.collect{ h -> row.find{it.name.sameAs(h)}?.value }.join(' | ')} |
  """.trim()}.join('\n')}
  """
  }

  static boolean usesTemplate(Scenario scenario) {
    scenario.template.value.orElse(false)
  }

  static String categoryOf(Step step) {
    switch (step.category.value.orElse('').toLowerCase()) {
      case 'given': return 'Given'
      case 'when' : return 'When'
      case 'then' : return 'Then'
      case 'and'  : return 'And'
      case '前提' : return 'Given'
      case '操作' : return 'When'
      case '結果' : return 'Then'
      default : return 'When'
    }
  }

  private static String opts(Step step) {
    List<String> opts = []
    if (!step.timeoutSec.empty) {
      opts << "タイムアウト:${step.timeoutSec}"
    }
    if (step.takesScreenshot.value.orElse(false)) {
      opts << "スクリーンショット"
    }
    if (!step.saveResultTo.empty) {
      opts << "結果保管先:${step.saveResultTo}"
    }
    opts.collect{"【${it}】"}.join('')
  }

  String getRelPath() {
    "${feature.fqn.replace('.', '/')}.feature"
  }
}
