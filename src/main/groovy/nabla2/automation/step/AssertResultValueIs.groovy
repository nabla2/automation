package nabla2.automation.step

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.transform.Canonical

@Canonical
class AssertResultValueIs implements Step {

  String paramName
  String expectedValueJson

  @Override
  void execute(ScenarioContext context) {
    Object expectedValue = new ObjectMapper().readValue(expectedValueJson, Object)
    context.doWith {
      results.get(paramName).with {
        assert it == expectedValue
      }
      true
    }
  }
}
