package nabla2.automation.step

import cucumber.api.Scenario
import geb.Browser
import geb.Page
import geb.navigator.Navigator
import groovy.transform.Canonical
import nabla2.metamodel.datatype.UrlWithPlaceholder
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot

@Canonical
class ScenarioContext {
  Browser browser
  Map<String, Page> pages
  Map<String, Object> results
  Scenario cucumberScenario
  Map<String, String> namedParams
  List<String> params

  private static final OPTS_TIMEOUT          = 'タイムアウト'
  private static final OPTS_TAKES_SCREENSHOT = 'スクリーンショット'
  private static final OPTS_SAVE_RESULT_TO   = '結果保管先'

  ScenarioContext opts(String optionString) {
    List<String> params = []
    Map<String, String> namedParams = [:]
    optionString
    .replaceAll(/(^【|】$)/, '')
    .split(/】\s*【/)
    .each {
      if (it.contains(':')) {
        it.split(':').with {
          namedParams.put(it.first(), it.last())
        }
      }
      else {
        params.add(it)
      }
    }
    new ScenarioContext(
      browser, pages, results, cucumberScenario, namedParams, params
    )
  }

  boolean needsScreenshot() {
    params.any{ it == OPTS_TAKES_SCREENSHOT }
  }

  ScenarioContext takeScreenshot() {
    byte[] screenshot = ((TakesScreenshot) browser.driver).getScreenshotAs(OutputType.BYTES)
    cucumberScenario.embed(screenshot, "image/png");
    this
  }

  Optional<String> saveResultTo() {
    Optional.ofNullable(namedParams.get(OPTS_SAVE_RESULT_TO))
  }

  ScenarioContext doWith(@DelegatesTo(ScenarioContext) Closure<?> task) {
    Closure<ScenarioContext> closure = (Closure)task.clone()
    closure.delegate = this
    closure.resolveStrategy = Closure.DELEGATE_FIRST

    double timeout = new BigDecimal(
      namedParams.getOrDefault(OPTS_TIMEOUT, "5")
    ).doubleValue()

    browser.page.init(browser)
    browser.waitFor(timeout) {
      def result = closure.call()
      saveResultTo().ifPresent { param ->
        results[param] = result
      }
      result
    }

    if (needsScreenshot()) takeScreenshot()
    this
  }

  Page page(String pageName) {
    pages.get(pageName).with {
      if (!it) throw new IllegalStateException(
        "unknown page name: ${pageName}"
      )
      it
    }
  }

  List<String> at(String pageName) {
    Page page = page(pageName)
    browser.page page
    String expected = normalizeUrl(browser.baseUrl + '/' + page.pageUrl)
    String current  = normalizeUrl(browser.currentUrl)
    UrlWithPlaceholder url = new UrlWithPlaceholder(expected)
    assert url.sameAs(current)
    url.capture(current)
  }

  static String normalizeUrl(String url) {
    url.replaceAll(/\/\/+/, '/')
       .replaceAll(/\?.*$/, '')
       .replaceAll(/\/$/, '')
       .replaceAll(/\:[0-9]+/, '')
       .replaceAll(/^https?\:\//, 'http://')
  }

  Navigator item(String itemName) {
    browser.page.init(browser)
    browser.page[itemName].with {
      if (!it || !(it instanceof Navigator)) throw new IllegalStateException(
        "Unknown page item: ${itemName}"
      )
      (Navigator) it
    }
  }
}
