package nabla2.automation.step.ui

import geb.navigator.Navigator
import groovy.transform.Canonical
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Step

@Canonical
class Choose implements Step {

  String itemName
  String selected

  @Override
  void execute(ScenarioContext context) {
    context.doWith {
      Navigator select = item(itemName).with {
        it.is('select') ? it : it.find('select').first()
      }
      select.value(selected)
    }
  }
}
