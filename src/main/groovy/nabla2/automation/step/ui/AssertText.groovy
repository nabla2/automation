package nabla2.automation.step.ui

import groovy.transform.Canonical
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Step

@Canonical
class AssertText implements Step {

  String itemName
  String text

  @Override
  void execute(ScenarioContext context) {
    context.doWith {
      item(itemName).text() == text
    }
  }
}
