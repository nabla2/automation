package nabla2.automation.step.ui

import groovy.transform.Canonical
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Step

@Canonical
class AssertDisabled implements Step {

  String itemName

  @Override
  void execute(ScenarioContext context) {
    context.doWith {
      item(itemName).attr("disabled")
    }
  }
}
