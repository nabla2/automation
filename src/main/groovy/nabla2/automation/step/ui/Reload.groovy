package nabla2.automation.step.ui

import groovy.transform.Canonical
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Step

@Canonical
class Reload implements Step {

  @Override
  void execute(ScenarioContext context) {
    context.doWith{
      browser.getDriver().navigate().refresh()
      return true
    }
  }
}
