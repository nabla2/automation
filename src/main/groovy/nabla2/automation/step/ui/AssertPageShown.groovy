package nabla2.automation.step.ui

import groovy.transform.Canonical
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Step

@Canonical
class AssertPageShown implements Step {

  String pageName

  @Override
  void execute(ScenarioContext context) {
    context.doWith {
      at pageName
    }
  }
}
