package nabla2.automation.step.ui

import groovy.transform.Canonical
import nabla2.automation.step.ScenarioContext
import nabla2.automation.step.Step

@Canonical
class OpenPage implements Step {

  String pageName

  @Override
  void execute(ScenarioContext context) {
    context.doWith {
      browser.to page(pageName)
    }
  }
}
