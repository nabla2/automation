package nabla2.automation.step

interface Step {
  void execute(ScenarioContext context)
}