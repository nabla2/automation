package nabla2.automation.step

import groovy.transform.Canonical

@Canonical
class Wait implements Step {

  String waitSec

  @Override
  void execute(ScenarioContext context) {
    new BigDecimal(waitSec).multiply(1000).with {
      Thread.sleep(longValue())
    }
  }
}
