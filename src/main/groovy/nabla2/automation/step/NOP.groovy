package nabla2.automation.step

import groovy.transform.Canonical

@Canonical
class NOP implements Step {
  String arg1
  String arg2
  String arg3
  String arg4
  String arg5

  void execute(ScenarioContext context) {
    //nop!!
  }
}
