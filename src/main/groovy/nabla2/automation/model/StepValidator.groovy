package nabla2.automation.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * ステップバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class StepValidator implements Validator<Step> {
  Observable<ConstraintViolation<Step>> validate(Observable<Table> table$) {
    Observable<Step> entity$ = Step.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Step>> validateUniqueConstraint(Observable<Step> entity$) {
    Map<String, Step> entityOf = [:]
    entity$.map { entity ->
      String key = JsonOutput.toJson([
        featureName : entity.featureName.value.map{it.toString()}.get(),
        scenarioName : entity.scenarioName.value.map{it.toString()}.get(),
        order : entity.order.value.map{it.toString()}.get(),
      ])
      Step another = entityOf[key]
      entityOf[key] = entity
      Optional.ofNullable(!another ? null : new ConstraintViolation<Step>(
        causedBy :[another, entity],
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '機能名, シナリオ名, 実行順',
          'ステップ',
          another,
          entity
        )
      ))
    }.filter {
      it.isPresent()
    }.map {
      it.get()
    }
  }

  static Observable<ConstraintViolation<Step>> validateCardinalityConstraint(Observable<Step> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.scenario == null) {
        violations.add(new ConstraintViolation<Step>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'シナリオ',
            "機能名=${entity.featureName}, 名称=${entity.scenarioName}",
            entity,
          )
        ))
      }
      if (entity.stepDefinition == null) {
        violations.add(new ConstraintViolation<Step>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'ステップ定義',
            "機能名=${entity.featureName}, 内容=${entity.description}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Step>> validatePropertyType(Observable<Step> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.scenarioName.messageIfInvalid.map{['シナリオ名', it]}.orElse(null),
        entity.order.messageIfInvalid.map{['実行順', it]}.orElse(null),
        entity.category.messageIfInvalid.map{['区分', it]}.orElse(null),
        entity.description.messageIfInvalid.map{['内容', it]}.orElse(null),
        entity.takesScreenshot.messageIfInvalid.map{['スクリーンショット', it]}.orElse(null),
        entity.timeoutSec.messageIfInvalid.map{['タイムアウト(秒)', it]}.orElse(null),
        entity.saveResultTo.messageIfInvalid.map{['結果保管先', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Step>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Step>> validateRequiredProperty(Observable<Step> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.scenarioName.literal.map{''}.orElse('シナリオ名'),
        entity.order.literal.map{''}.orElse('実行順'),
        entity.category.literal.map{''}.orElse('区分'),
        entity.description.literal.map{''}.orElse('内容'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Step>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}