package nabla2.automation.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * シナリオバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ScenarioValidator implements Validator<Scenario> {
  Observable<ConstraintViolation<Scenario>> validate(Observable<Table> table$) {
    Observable<Scenario> entity$ = Scenario.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Scenario>> validateUniqueConstraint(Observable<Scenario> entity$) {
    Map<String, Scenario> entityOf = [:]
    entity$.map { entity ->
      String key = JsonOutput.toJson([
        featureName : entity.featureName.value.map{it.toString()}.get(),
        name : entity.name.value.map{it.toString()}.get(),
      ])
      Scenario another = entityOf[key]
      entityOf[key] = entity
      Optional.ofNullable(!another ? null : new ConstraintViolation<Scenario>(
        causedBy :[another, entity],
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '機能名, 名称',
          'シナリオ',
          another,
          entity
        )
      ))
    }.filter {
      it.isPresent()
    }.map {
      it.get()
    }
  }

  static Observable<ConstraintViolation<Scenario>> validateCardinalityConstraint(Observable<Scenario> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.feature == null) {
        violations.add(new ConstraintViolation<Scenario>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '機能',
            "名称=${entity.featureName}",
            entity,
          )
        ))
      }
      if (entity.steps == null) {
        violations.add(new ConstraintViolation<Scenario>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'ステップ',
            "機能名=${entity.featureName}, シナリオ名=${entity.name}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Scenario>> validatePropertyType(Observable<Scenario> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.order.messageIfInvalid.map{['実行順', it]}.orElse(null),
        entity.template.messageIfInvalid.map{['テンプレート', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Scenario>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Scenario>> validateRequiredProperty(Observable<Scenario> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.name.literal.map{''}.orElse('名称'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Scenario>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}