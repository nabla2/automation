package nabla2.automation.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.OrdinalNumber
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.automation.model.Scenario
/**
 * シナリオ置換部分
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class ScenarioPlaceholder {

  /** 論理名 */
  static final String ENTITY_NAME = "シナリオ置換部分"

  // ----- プロパティ定義 ------ //

  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * シナリオ名
   */
  SingleLineText scenarioName
  /**
   * ケース番号
   */
  OrdinalNumber caseNumber
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 値
   */
  SingleLineText value
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * シナリオ
   */
  @Memoized
  @JsonIgnore
  Scenario getScenario() {
    Scenario.from(_table$).filter {
      this.scenarioName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<ScenarioPlaceholder> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new ScenarioPlaceholder(
            featureName : new SingleLineText(row['機能名']),
            scenarioName : new SingleLineText(row['シナリオ名']),
            caseNumber : new OrdinalNumber(row['ケース番号']),
            name : new SingleLineText(row['名称']),
            value : new SingleLineText(row['値']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}