package nabla2.automation.model.ui

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.automation.model.Feature
import nabla2.automation.model.ui.PageItem
/**
 * 画面
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Page {

  /** 論理名 */
  static final String ENTITY_NAME = "画面"

  // ----- プロパティ定義 ------ //

  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * パス
   */
  UrlWithPlaceholder path
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 機能
   */
  @Memoized
  @JsonIgnore
  Feature getFeature() {
    Feature.from(_table$).filter {
      this.featureName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 画面項目リスト
   */
  @Memoized
  @JsonIgnore
  List<PageItem> getItems() {
    PageItem.from(_table$).filter {
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.pageName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Page> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Page(
            featureName : new SingleLineText(row['機能名']),
            name : new SingleLineText(row['名称']),
            path : new UrlWithPlaceholder(row['パス']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}