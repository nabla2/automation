package nabla2.automation.model.ui

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.automation.model.ui.Page
/**
 * 画面項目
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class PageItem {

  /** 論理名 */
  static final String ENTITY_NAME = "画面項目"

  // ----- プロパティ定義 ------ //

  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 画面名
   */
  SingleLineText pageName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * セレクタ
   */
  SingleLineText selector
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 画面
   */
  @Memoized
  @JsonIgnore
  Page getPage() {
    Page.from(_table$).filter {
      this.featureName.sameAs(it.featureName) &&
      this.pageName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<PageItem> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new PageItem(
            featureName : new SingleLineText(row['機能名']),
            pageName : new SingleLineText(row['画面名']),
            name : new SingleLineText(row['名称']),
            selector : new SingleLineText(row['セレクタ']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}