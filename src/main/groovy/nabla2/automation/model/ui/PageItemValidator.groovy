package nabla2.automation.model.ui

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 画面項目バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PageItemValidator implements Validator<PageItem> {
  Observable<ConstraintViolation<PageItem>> validate(Observable<Table> table$) {
    Observable<PageItem> entity$ = PageItem.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<PageItem>> validateUniqueConstraint(Observable<PageItem> entity$) {
    Map<String, PageItem> entityOf = [:]
    entity$.map { entity ->
      String key = JsonOutput.toJson([
        featureName : entity.featureName.value.map{it.toString()}.get(),
        pageName : entity.pageName.value.map{it.toString()}.get(),
        name : entity.name.value.map{it.toString()}.get(),
        selector : entity.selector.value.map{it.toString()}.get(),
      ])
      PageItem another = entityOf[key]
      entityOf[key] = entity
      Optional.ofNullable(!another ? null : new ConstraintViolation<PageItem>(
        causedBy :[another, entity],
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '機能名, 画面名, 名称, セレクタ',
          '画面項目',
          another,
          entity
        )
      ))
    }.filter {
      it.isPresent()
    }.map {
      it.get()
    }
  }

  static Observable<ConstraintViolation<PageItem>> validateCardinalityConstraint(Observable<PageItem> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.page == null) {
        violations.add(new ConstraintViolation<PageItem>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '画面',
            "機能名=${entity.featureName}, 名称=${entity.pageName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageItem>> validatePropertyType(Observable<PageItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.pageName.messageIfInvalid.map{['画面名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.selector.messageIfInvalid.map{['セレクタ', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<PageItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageItem>> validateRequiredProperty(Observable<PageItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.pageName.literal.map{''}.orElse('画面名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.selector.literal.map{''}.orElse('セレクタ'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<PageItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}