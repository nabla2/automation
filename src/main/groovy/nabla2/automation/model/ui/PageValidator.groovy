package nabla2.automation.model.ui

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 画面バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PageValidator implements Validator<Page> {
  Observable<ConstraintViolation<Page>> validate(Observable<Table> table$) {
    Observable<Page> entity$ = Page.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Page>> validateUniqueConstraint(Observable<Page> entity$) {
    Map<String, Page> entityOf = [:]
    entity$.map { entity ->
      String key = JsonOutput.toJson([
        featureName : entity.featureName.value.map{it.toString()}.get(),
        name : entity.name.value.map{it.toString()}.get(),
      ])
      Page another = entityOf[key]
      entityOf[key] = entity
      Optional.ofNullable(!another ? null : new ConstraintViolation<Page>(
        causedBy :[another, entity],
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '機能名, 名称',
          '画面',
          another,
          entity
        )
      ))
    }.filter {
      it.isPresent()
    }.map {
      it.get()
    }
  }

  static Observable<ConstraintViolation<Page>> validateCardinalityConstraint(Observable<Page> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.feature == null) {
        violations.add(new ConstraintViolation<Page>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '機能',
            "名称=${entity.featureName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Page>> validatePropertyType(Observable<Page> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.path.messageIfInvalid.map{['パス', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Page>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Page>> validateRequiredProperty(Observable<Page> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.path.literal.map{''}.orElse('パス'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Page>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}