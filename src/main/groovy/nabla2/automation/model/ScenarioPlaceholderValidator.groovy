package nabla2.automation.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * シナリオ置換部分バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ScenarioPlaceholderValidator implements Validator<ScenarioPlaceholder> {
  Observable<ConstraintViolation<ScenarioPlaceholder>> validate(Observable<Table> table$) {
    Observable<ScenarioPlaceholder> entity$ = ScenarioPlaceholder.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<ScenarioPlaceholder>> validateUniqueConstraint(Observable<ScenarioPlaceholder> entity$) {
    Map<String, ScenarioPlaceholder> entityOf = [:]
    entity$.map { entity ->
      String key = JsonOutput.toJson([
        featureName : entity.featureName.value.map{it.toString()}.get(),
        scenarioName : entity.scenarioName.value.map{it.toString()}.get(),
        caseNumber : entity.caseNumber.value.map{it.toString()}.get(),
        name : entity.name.value.map{it.toString()}.get(),
      ])
      ScenarioPlaceholder another = entityOf[key]
      entityOf[key] = entity
      Optional.ofNullable(!another ? null : new ConstraintViolation<ScenarioPlaceholder>(
        causedBy :[another, entity],
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '機能名, シナリオ名, ケース番号, 名称',
          'シナリオ置換部分',
          another,
          entity
        )
      ))
    }.filter {
      it.isPresent()
    }.map {
      it.get()
    }
  }

  static Observable<ConstraintViolation<ScenarioPlaceholder>> validateCardinalityConstraint(Observable<ScenarioPlaceholder> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.scenario == null) {
        violations.add(new ConstraintViolation<ScenarioPlaceholder>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'シナリオ',
            "名称=${entity.scenarioName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ScenarioPlaceholder>> validatePropertyType(Observable<ScenarioPlaceholder> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.scenarioName.messageIfInvalid.map{['シナリオ名', it]}.orElse(null),
        entity.caseNumber.messageIfInvalid.map{['ケース番号', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.value.messageIfInvalid.map{['値', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<ScenarioPlaceholder>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ScenarioPlaceholder>> validateRequiredProperty(Observable<ScenarioPlaceholder> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.scenarioName.literal.map{''}.orElse('シナリオ名'),
        entity.caseNumber.literal.map{''}.orElse('ケース番号'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.value.literal.map{''}.orElse('値'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<ScenarioPlaceholder>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}