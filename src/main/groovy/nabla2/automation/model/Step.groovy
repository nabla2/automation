package nabla2.automation.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.OrdinalNumber
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.UnsignedNumber
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.automation.model.Scenario
import nabla2.automation.model.StepDefinition
/**
 * ステップ
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Step {

  /** 論理名 */
  static final String ENTITY_NAME = "ステップ"

  // ----- プロパティ定義 ------ //

  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * シナリオ名
   */
  SingleLineText scenarioName
  /**
   * 実行順
   */
  OrdinalNumber order
  /**
   * 区分
   */
  SingleLineText category
  /**
   * 内容
   */
  ParameterizedText description
  /**
   * スクリーンショット
   */
  BooleanFlag takesScreenshot
  /**
   * タイムアウト(秒)
   */
  UnsignedNumber timeoutSec
  /**
   * 結果保管先
   */
  SingleLineText saveResultTo
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * シナリオ
   */
  @Memoized
  @JsonIgnore
  Scenario getScenario() {
    Scenario.from(_table$).filter {
      this.featureName.sameAs(it.featureName) &&
      this.scenarioName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * ステップ定義
   */
  @Memoized
  @JsonIgnore
  StepDefinition getStepDefinition() {
    StepDefinition.from(_table$).filter {
      this.featureName.sameAs(it.featureName) &&
      this.description.sameAs(it.description)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Step> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Step(
            featureName : new SingleLineText(row['機能名']),
            scenarioName : new SingleLineText(row['シナリオ名']),
            order : new OrdinalNumber(row['実行順']),
            category : new SingleLineText(row['区分']),
            description : new ParameterizedText(row['内容']),
            takesScreenshot : new BooleanFlag(row['スクリーンショット']),
            timeoutSec : new UnsignedNumber(row['タイムアウト(秒)']),
            saveResultTo : new SingleLineText(row['結果保管先']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}