package nabla2.automation.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.OrdinalNumber
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.automation.model.Feature
import nabla2.automation.model.Step
import nabla2.automation.model.ScenarioPlaceholder
/**
 * シナリオ
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Scenario {

  /** 論理名 */
  static final String ENTITY_NAME = "シナリオ"

  // ----- プロパティ定義 ------ //

  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 実行順
   */
  OrdinalNumber order
  /**
   * テンプレート
   */
  BooleanFlag template
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 機能
   */
  @Memoized
  @JsonIgnore
  Feature getFeature() {
    Feature.from(_table$).filter {
      this.featureName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * ステップリスト
   */
  @Memoized
  @JsonIgnore
  List<Step> getSteps() {
    Step.from(_table$).filter {
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.scenarioName)
    }.toList().blockingGet()
  }
  /**
   * シナリオ置換部分リスト
   */
  @Memoized
  @JsonIgnore
  List<ScenarioPlaceholder> getPlaceholders() {
    ScenarioPlaceholder.from(_table$).filter {
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.scenarioName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Scenario> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Scenario(
            featureName : new SingleLineText(row['機能名']),
            name : new SingleLineText(row['名称']),
            order : new OrdinalNumber(row['実行順']),
            template : new BooleanFlag(row['テンプレート']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}