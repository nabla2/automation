package nabla2.automation.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.automation.model.Feature
/**
 * ステップ定義
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class StepDefinition {

  /** 論理名 */
  static final String ENTITY_NAME = "ステップ定義"

  // ----- プロパティ定義 ------ //

  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 内容
   */
  ParameterizedText description
  /**
   * 区分
   */
  SingleLineText category
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 機能
   */
  @Memoized
  @JsonIgnore
  Feature getFeature() {
    Feature.from(_table$).filter {
      this.featureName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<StepDefinition> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new StepDefinition(
            featureName : new SingleLineText(row['機能名']),
            description : new ParameterizedText(row['内容']),
            category : new SingleLineText(row['区分']),
            namespace : new JavaNamespace(row['名前空間']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}